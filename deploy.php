<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'demo-ci-laravel');

// Project repository
set('repository', 'git@gitlab.com:pht/demo-ci-laravel.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts
host('test-vultr-ubuntu-18')
    ->set('deploy_path', '~/{{application}}');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

desc('Make env');
task('deploy:make_env', function () {
    $output = run(
        'if [ ! -f {{deploy_path}}/shared/.env ]; then cp {{release_path}}/.env.example {{release_path}}/.env; fi'
    );
    writeln('<info>' . $output . '</info>');
});

desc('Deploy init');
task('deploy:init', [
    'deploy:info',
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:vendors',
    'deploy:make_env',
    'deploy:shared',
    'deploy:writable',
    'cleanup',
]);

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    // 'artisan:optimize',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);
